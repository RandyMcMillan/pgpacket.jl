@testset "PGP binary" begin
    @testset "Public Key" begin
        x = parse(BigInt, "f05314566c9bfc8d8cf463a7a01e7735245d588a60dd874f09a9636620abb314", base=16)
        y = parse(BigInt, "6bda245d43cbbe019ab1ad74316d675dd858cdd776820969bcc21bbccbd3a661", base=16)
        point = S256Point(x, y)
        packet = bin2packet("pubkey.bin")[1]
        @test packet.body.pubkey == point
    end
    @testset "Secret Key" begin
        secret = parse(BigInt, "e3b8149f0ace21a5b7ec1fea2770aac7eb9bf739f577c040e00bec1e97a8e9fe", base=16)
        packet = bin2packet("privkey.bin")[1]
        @test packet.body.specifics[4] == secret
    end
    @testset "Signature Key" begin
        r = parse(BigInt, "6d9fbf978cd1b3c46ec2ad03ed5c3e511ca634ff4208379eadd6a6e25a1cdefd", base=16)
        s = parse(BigInt, "eb48de6d50ead23877e07d171c5d5563d0b0e8fce243ae7ab8b20adf379925d3", base=16)
        sig = Signature(r, s)
        packet = bin2packet("z.bin.sig")[1]
        @test packet.body.sig == sig
    end
end
