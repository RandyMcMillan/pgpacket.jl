const SIGNATURE_TYPES = Dict([
   (0x00, "Signature of a binary document"),
   (0x01, "Signature of a canonical text document"),
   (0x02, "Standalone signature"),
   (0x10, "Generic certification of a User ID and Public-Key packet"),
   (0x11, "Persona certification of a User ID and Public-Key packet"),
   (0x12, "Casual certification of a User ID and Public-Key packet"),
   (0x13, "Positive certification of a User ID and Public-Key packet"),
   (0x18, "Subkey Binding Signature"),
   (0x19, "Primary Key Binding Signature"),
   (0x1f, "Signature directly on a key"),
   (0x20, "Key revocation signature"),
   (0x28, "Subkey revocation signature"),
   (0x30, "Certification revocation signature"),
   (0x40, "Timestamp signature"),
   (0x50, "Third-Party Confirmation signature")
])

const HASH_ALGO = Dict([
    (1, "MD5"),
    (2, "SHA1"),
    (3, "RIPEMD160"),
    # 4 to 7 - Reserved
    (8, "SHA256"),
    (9, "SHA384"),
    (10, "SHA512"),
    (11, "SHA224")
    # 100 to 110 - Private/Experimental algorithm
])

const SUBPACKET_TYPE_OCTET = Dict([
    (0, "Reserved"),
    (1, "Reserved"),
    (2, "Signature Creation Time"),
    (3, "Signature Expiration Time"),
    (4, "Exportable Certification"),
    (5, "Trust Signature"),
    (6, "Regular Expression"),
    (7, "Revocable"),
    (8, "Reserved"),
    (9, "Key Expiration Time"),
    (10, "Placeholder for backward compatibility"),
    (11, "Preferred Symmetric Algorithms"),
    (12, "Revocation Key"),
    (13, "Reserved"),
    (14, "Reserved"),
    (15, "Reserved"),
    (16, "Issuer"),
    (17, "Reserved"),
    (18, "Reserved"),
    (19, "Reserved"),
    (20, "Notation Data"),
    (21, "Preferred Hash Algorithms"),
    (22, "Preferred Compression Algorithms"),
    (23, "Key Server Preferences"),
    (24, "Preferred Key Server"),
    (25, "Primary User ID"),
    (26, "Policy URI"),
    (27, "Key Flags"),
    (28, "Signer's User ID"),
    (29, "Reason for Revocation"),
    (30, "Features"),
    (31, "Signature Target"),
    (32, "Embedded Signature"),
    (33, "Issuer Fingerprint")
    # 100 To 110 - Private or experimental
])

abstract type AbstractSignaturePacket <: PGPBody end

struct SignatureSubPacket <: AbstractSignaturePacket
    type::Integer
    body::Array{UInt8,1}
    SignatureSubPacket(type, body) = new(type, body)
end

function show(io::IO, z::SignatureSubPacket)
    print(io, "\n    ", SUBPACKET_TYPE_OCTET[z.type], "\n    ", bytes2hex(z.body))
end

"""
IOBuffer -> Array{SignatureSubPacket,1}
"""
function parsesigsub(bin::Array{UInt8,1}, byte_length::Integer)
    io, i, result = IOBuffer(bin), 0, SignatureSubPacket[]
    while i < byte_length
        sublength = read(io, 1)[1]
        if sublength > 192
            error("not implemented")
        end
        type = read(io, 1)[1]
        data = read(io, sublength - 1)
        push!(result, SignatureSubPacket(type, data))
        i += 0x01 + sublength
    end
    return result
end

struct SignaturePacket <: AbstractSignaturePacket
    version::Integer
    type::Integer
    hash_algo::Integer
    hashed_subpacket::Array{SignatureSubPacket,1}
    unhashed_subpacket::Array{SignatureSubPacket,1}
    hash_left::Array{UInt8,1}
    sig::Any
    SignaturePacket(version, type, hash_algo, hashed_subpacket, unhashed_subpacket, hash_left, sig) = new(version, type, hash_algo, hashed_subpacket, unhashed_subpacket, hash_left, sig)
end

function show(io::IO, z::SignaturePacket)
    print(io, " Version : ", z.version,
            "\n Type : ", HASH_ALGO[z.hash_algo],
            "\n", z.hashed_subpacket,
            "\n", z.unhashed_subpacket,
            "\n Hash left : ", bytes2hex(z.hash_left),
            "\n ", z.sig)
end

"""
IOBuffer -> ECC.Signature
"""
function parsesig(io::IOBuffer)
    rlen = readscalar(read(io, 2))
    r = read(io, scalar2bytes(rlen))
    slen = readscalar(read(io, 2))
    s = read(io, scalar2bytes(slen))
    return ECC.Signature(bytes2int(r), bytes2int(s))
end

"""
Array{UInt8,1}, PGPHeader -> SignaturePacket
"""
function bin2sig(io::IOBuffer, header::PGPHeader)
    version = read(io, 1)[1]
    if version == 3
        error("version 3 not implemented")
    elseif version == 4
        type = read(io, 1)[1]
        pubkey_algo = read(io, 1)[1]
        hash_algo = read(io, 1)[1]
        hash_length = readscalar(read(io, 2))
        hashed_subpacket = parsesigsub(read(io, hash_length), hash_length)
        unhash_length = readscalar(read(io, 2))
        unhashed_subpacket = parsesigsub(read(io, unhash_length), unhash_length)
        hash_left = read(io, 2)
        sig = parsesig(io)
    end
    return SignaturePacket(version, type, hash_algo, hashed_subpacket, unhashed_subpacket, hash_left, sig)
end
