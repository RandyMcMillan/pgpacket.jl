const PUBLIC_KEY_ALGORITHM = Dict([
   (1, "RSA (Encrypt or Sign) [HAC]"),
   (2, "RSA Encrypt-Only [HAC]"),
   (3, "RSA Sign-Only [HAC]"),
   (16, "Elgamal (Encrypt-Only) [ELGAMAL] [HAC]"),
   (17, "DSA (Digital Signature Algorithm) [FIPS186] [HAC]"),
   (18, "ECDH public key algorithm"),
   (19, "ECDSA public key algorithm [FIPS186]"),
   (20, "Reserved (formerly Elgamal Encrypt or Sign)"),
   (21, "Reserved for Diffie-Hellman (X9.42, as defined for IETF-S/MIME)"),
   (22, "EdDSA [RFC8032]"),
   (23, "Reserved for AEDH"),
   (24, "Reserved for AEDSA")
   # 100 to 110 - Private/Experimental algorithm
])

const SYMMETRICKEY_ALGORITHM = Dict([
   (0, "Plaintext or unencrypted data"),
   (1, "IDEA [IDEA]"),
   (2, "TripleDES (DES-EDE, [SCHNEIER] [HAC] - 168 bit key derived from 192)"),
   (3, "CAST5 (128 bit key, as per [RFC2144])"),
   (4, "Blowfish (128 bit key, 16 rounds) [BLOWFISH]"),
   (5, "Reserved"),
   (6, "Reserved"),
   (7, "AES with 128-bit key [AES]"),
   (8, "AES with 192-bit key"),
   (9, "AES with 256-bit key"),
   (10, "Twofish with 256-bit key [TWOFISH]"),
   (11, "Camellia with 128-bit key [RFC3713]"),
   (12, "Camellia with 192-bit key"),
   (13, "Camellia with 256-bit key")
   # 100--110 | Private/Experimental algorithm
])

const PACKET = Dict([
    (0, "Reserved - a packet tag MUST NOT have this value"),
    (1, "Public-Key Encrypted Session Key Packet"),
    (2, "Signature Packet"),
    (3, "Symmetric-Key Encrypted Session Key Packet"),
    (4, "One-Pass Signature Packet"),
    (5, "Secret-Key Packet"),
    (6, "Public-Key Packet"),
    (7, "Secret-Subkey Packet"),
    (8, "Compressed Data Packet"),
    (9, "Symmetrically Encrypted Data Packet"),
    (10, "Marker Packet"),
    (11, "Literal Data Packet"),
    (12, "Trust Packet"),
    (13, "User ID Packet"),
    (14, "Public-Subkey Packet"),
    (17, "User Attribute Packet"),
    (18, "Sym. Encrypted and Integrity Protected Data Packet"),
    (19, "Modification Detection Code Packet"),
    (60, "Private or Experimental Values"),
    (61, "Private or Experimental Values"),
    (62, "Private or Experimental Values"),
    (63, "Private or Experimental Values")
])
